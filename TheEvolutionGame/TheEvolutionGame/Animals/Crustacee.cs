﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Crustacee : VertebratAcvatic
    {
        public int NrPicioare { get; private set; }
        public int NrClesti { get; private set; }

        public Crustacee() { }
        public Crustacee(string Name, int Age, int Speed, string EatCategory, int Depth, string TypeOfWater,  int NrPicioare, int NrClesti) : base(Name, Age, Speed, EatCategory, Depth, TypeOfWater)
        {
            this.NrPicioare = NrPicioare;
            this.NrClesti = NrClesti;
        }
        public override string Description()
        {
            return base.Description() + $"I have {NrPicioare} legs and {NrClesti} tongs";
        }
    }
}

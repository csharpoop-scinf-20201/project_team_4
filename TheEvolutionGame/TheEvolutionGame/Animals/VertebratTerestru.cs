﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class VertebratTerestru : Animal, ITerestru
    {
        public string LandForm { get; set; }

        public VertebratTerestru() { }

        public VertebratTerestru(string Name, int Age, int Speed, string EatCategory, string LandForm) : base(Name, Age, Speed, EatCategory)
        {
            this.LandForm = LandForm;
        }
        public override string Description()
        {
            return $"I am an terestrial animal with the name {Name}, I am alive for {Age} years and " +
                $"I live in/on {LandForm}!";
        }
    }
}

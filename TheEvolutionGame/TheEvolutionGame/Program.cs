﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Administrate;
using TheEvolutionGame.Animals;
using TimeChanges;

namespace TheEvolutionGame.Animals
{
    class Program
    { 
        static void Main(string[] args)
        {
            World world = new World();
            world.Populate();

            int id = 0;

            foreach (var animal in world.Animals)
            {
                id += 1;
                Console.WriteLine(id + ". " + animal.Description() + Environment.NewLine);
            }
            Console.WriteLine("-----------------------------------------------");

            Animal newAnimal = Adapt.DeEvolve(world.Animals[2]);
            Console.WriteLine();
            Console.WriteLine("New type: " + newAnimal.GetType() + Environment.NewLine + newAnimal.Description());

            Console.WriteLine("-----------------------------------------------");

            Animal newAnimal2 = Adapt.Evolve(world.Animals[2]);
            Console.WriteLine("New type: " + newAnimal2.GetType() + Environment.NewLine + newAnimal2.Description());

            Console.WriteLine("-----------------------------------------------");

            world.AddAnimal(newAnimal);

            Animal baby = Reproduce.Do(world.Animals[7], world.Animals[8]);
            Console.WriteLine("New baby: " + baby.GetType() + Environment.NewLine + baby.Description());

            Console.WriteLine("-----------------------------------------------");

            world.Animals[7].Friends();

            world.AddElephant();


            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();

        }

        private static void Explore()
        {
            while (true)
            {
                SeeOptions();
                string option = Console.ReadLine();
                switch (option)
                {
                    case "1": { Console.WriteLine(); break; }
                    case "2": { Console.WriteLine(); break; }
                    case "3": { Console.WriteLine(); break; }
                    default: { Console.WriteLine("Wrong choise."); break; }
                }
                
            }
        }


        private static string SeeOptions()
        {
            string response = "Do something:\n";
            response += "1. Evolve a species.\n";
            response += "2. DeEvolve a species.\n";
            response += "3. Add animal.\n";
            return response;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class VertebratAerianTerestru : Animal, ITerestru, IAerian
    {
        public string LandForm { get; set; }
        public int WingsDim { get; set; }
        public int Altitude { get; set; }

        public VertebratAerianTerestru() { }

        public VertebratAerianTerestru(string Name, int Age, int Speed, string EatCategory, int WingsDim, int Altitude, string LandForm) : base(Name, Age, Speed, EatCategory)
        {
            this.Altitude = Altitude;
            this.WingsDim = WingsDim;
            this.LandForm = LandForm;
        }
        public override string Description()
        {
            return $"I am an aerian and terrestrial animal with the name {Name}, I am alive for {Age} years and " +
               $"I fly at {Altitude} meters but I also walk on land {LandForm}! I am {EatCategory}.";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Lebada : VertebratAerianTerestru
    {
        int Weight { get; set; }

        string CuloarePenaj { get; set; }

        int NrOua { get; set; }

        public Lebada(string Name, int Age, int Speed, string EatCategory,int Weight, string CuloarePenaj,
              int NrOua, int WingsDimension, int Altitude,string LandForm) : base(Name, Age, Speed, EatCategory, WingsDimension, Altitude, LandForm)
        {
            this.Weight = Weight;
            this.CuloarePenaj = CuloarePenaj;
            this.NrOua = NrOua;
        }
    

        public override string Description()
        {
            return base.Description() + $"I am a {Name} and I fly around 800 km with speed of {Speed} km / hour , I do around {NrOua} eggs and " +
                $"my weight is {Weight}";
        }
    }
}

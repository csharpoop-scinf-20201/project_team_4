﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    public abstract class Animal
    {
        public string Name { get; private set; }

        public int Age { get; private set; }

        public int Speed { get; private set; }

        public string EatCategory { get; private set; } //Omnivore, Herbivore, Carnivore

        public Animal(){}

        public Animal(string Name, int Age, int Speed, string EatCategory)
        {
            this.Name = Name;
            this.Age = Age;
            this.Speed = Speed;
            this.EatCategory = EatCategory;
            
        }

        public abstract string Description();

        public void Friends()
        {
            Console.WriteLine("Nature is animals' friend");
        }

        public void Enemies()
        {
            Console.WriteLine("Humans are animals' enemies");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class Vultur : VertebratAerianTerestru
    {
        public int NrOua { get; set; }

        public string SeImperecheaza { get; set; }

        public Vultur (string Name, int Age, int Speed, string EatCategory,
             int NrOua, string SeImperecheaza, int WingsDim, int Altitude, string LandForm) : base (Name,Age, Speed, EatCategory, WingsDim, Altitude, LandForm)
        {
            this.NrOua = NrOua;
            this.SeImperecheaza = SeImperecheaza;
        }

        public new string Friends()
        {
            return $"Well I don't have many firends! :( ";
        }
        public new string Enemies()
        {
            return $"No enemies also :)";
        }
        public override string Description()
        {
            return base.Description() + $" I like to eat meat, I do {NrOua} eggs and {SeImperecheaza} I have a match. ";
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Strut : VertebratTerestru
    {
        public int NrOua { get; set; }

        public string SeImperecheaza { get; set; }


        public Strut(string Name, int Age, int Speed, string EatCategory,
             int NrOua, string SeImperecheaza, string LandForm) : base(Name, Age, Speed, EatCategory, LandForm)
        {
            this.NrOua = NrOua;
            this.SeImperecheaza = SeImperecheaza;
        }
        public override string Description()
        {
            return base.Description() + $"I like to eat fish, fruits, vegetables so I am an {EatCategory} and I do {NrOua} " +
                $"eggs at each 2 days and {SeImperecheaza} I have a match ";
        }
    }
}

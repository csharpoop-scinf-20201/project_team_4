﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class MamiferAcvatic : VertebratAcvatic
    {
        public int PerioadaAer { get; set; }    //intervalul de timp la care iese sa ia aer

        public MamiferAcvatic()
        {

        }

        public MamiferAcvatic(string Name, int Age, int Speed, int Depth, int NrAripioare,
           int PerioadaAer) : base(Name, Age, Speed, Depth, NrAripioare)
        {
            this.PerioadaAer = PerioadaAer;
        }

        public override string Description()
        {
            return $"I am an aquatic vertebrate with the name {Name}, I am alive for {Age} years, " +
                $"I live at the depth {Depth} meters, I take a breath every {PerioadaAer} minutes!";
        }


    }
}

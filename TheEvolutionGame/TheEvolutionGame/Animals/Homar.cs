﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class Homar : Crustacee
    {
        public Homar() { }

        public Homar(string Name, int Age, int Speed, string EatCategory, int Depth, string TypeOfWater, int NrPicioare, int NrClesti) : base(Name, Age, Speed, EatCategory, Depth, TypeOfWater, NrPicioare, NrClesti)
        {
        }
        public override string Description()
        {
            return base.Description() + $"I have {NrPicioare} legs and {NrClesti} tongs";
        }
    }
}

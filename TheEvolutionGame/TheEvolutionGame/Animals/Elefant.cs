﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class Elefant : VertebratTerestru
    {        
        int Gestation { get; set; }
        int Weight { get; set; }

        public Elefant(string Name, int Age, int Speed, string EatCategory, int Weight, int gestation, string LandForm) : base(Name, Age, Speed, EatCategory, LandForm)
        {
            
            this.Gestation = gestation;
            this.Weight = Weight;
        }
        public override string Description()
        {
            return base.Description() + $"My speed is {Speed} km/ hour, I am {EatCategory}, my weight is {Weight} kg and I have a gestation of {Gestation} weeks!";
        }

    }
    
}

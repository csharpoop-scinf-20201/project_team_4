﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class VertebratAcvatic : Animal, IAcvatic
    {
        public int Depth { get ; set; }

        public string TypeOfWater { get; set; }

        public VertebratAcvatic(){}

        public VertebratAcvatic(string Name, int Age, int Speed, string EatCategory, int Depth, string TypeOfWater) : base(Name, Age, Speed, EatCategory)
        {
            this.Depth = Depth;
            this.TypeOfWater = TypeOfWater;
        }
        public override string Description()
        {
            return $"I am an aquatic animal with the name {Name}, I am alive for {Age} years and " +
                $"I live at the depth {Depth} meters in {TypeOfWater} water!";
        }
    }
}

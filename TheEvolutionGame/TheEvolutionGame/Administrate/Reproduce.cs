﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals;

namespace TheEvolutionGame.Administrate
{
    static class Reproduce
    {

        public static Animal Do (Animal mother, Animal father)
        {
            if(mother.GetType().IsAssignableFrom(father.GetType()) && !mother.GetType().IsAbstract && !mother.GetType().IsInterface)
            {
                if(mother.GetType() == typeof(Vultur))
                {
                    Vultur m = mother as Vultur;
                    Vultur f = father as Vultur;

                    return new Vultur(f.Name, 0, f.Speed, m.EatCategory, m.NrOua, f.SeImperecheaza, m.WingsDim, f.Altitude, m.LandForm);
                }else
                    return null; // De completat

            }
            else if (father.GetType().IsAssignableFrom(mother.GetType()) && !father.GetType().IsAbstract && !father.GetType().IsInterface)
            {
                if (father.GetType() == typeof(Elefant))
                {
                    Elefant m = mother as Elefant;
                    Elefant f = father as Elefant;

                    return new Elefant(f.Name, 0, f.Speed, m.EatCategory, 30, 6, f.LandForm);
                }
                else
                    return null; // De completat
            }
            else
                return null;
            
        }
    }
}

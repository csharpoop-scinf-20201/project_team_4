﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Antilopa : VertebratTerestru/// Andreea Dumitrasc
    {
        
        int Gestation{ get; set; }

        int NrBaby { get; set; }

        int Weight { get; set; }
        Antilopa() {}
        public Antilopa(string Name, int Age, int Speed, string EatCategory, int Weight,int gestationTime, int nrbaby, string LandForm)
            : base(Name, Age, Speed, EatCategory, LandForm)
        {
            this.Weight = Weight;
            this.Gestation = gestationTime;
            this.NrBaby = nrbaby;

        }
        public override string Description()
        {
            return base.Description() + $"I run with speed of {Speed} km / hour " +
                    $"I am {EatCategory} and my weight is {Weight} kg. I have {NrBaby} babies and weight {Weight}Kg";
        }
        public string  GestationTime()
        {
            return $"Gestation time is between {Gestation} weeks delivering just {NrBaby} baby...";
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals;

namespace TimeChanges
{
    public static class Adapt
    {
        public static Animal Evolve(Animal animal)
        {

            var listOfSupTypes = 
                AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => animal.GetType().IsAssignableFrom(type)); // tipurile superioare celui curent. GetType da tipul curent si vad ce tipuri le pot asigna lui. Interez prin ele si il aleg pe cel ce are BaseType == animal.GetType

            if (listOfSupTypes.Count() == 0)
                return animal;

            var directSupType = listOfSupTypes.Where(x => x.BaseType == animal.GetType()); // Gasesc tipurile superioare care au baza "directa" tipul curent


            Random rand = new Random();
            int index = rand.Next(0, directSupType.Count());
            var supType = directSupType.ElementAt(index);


            // caut cine e tipul curent
            if(animal.GetType() == typeof(Megalodon)) 
            {
                Megalodon m = animal as Megalodon;
                // Creez tipul superior
                if (supType == typeof(Rechin))
                {
                    return new Rechin("Rechin", m.Age, m.Speed, m.EatCategory, m.Depth, m.NrAripioare, m.Pradator, m.Comestibil, m.TypeOfWater, m.Lenght, m.NrDinti);
                }

            }

            return null;
        }

        public static Animal DeEvolve(Animal animal)
        {
            Console.WriteLine("Old type: " + animal.GetType() + Environment.NewLine);

           /* foreach (var element in
                AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.IsAssignableFrom(animal.GetType()))  ) // tipurile inferioare lui animal
            {
                Console.WriteLine(element.FullName);
            }*/


            if (animal.GetType().BaseType.IsAbstract || animal.GetType().BaseType.IsInterface)
            {
                return animal;
            }

            //Acum trebuie sa caut basetype-ul si sa creez noul obiect. Nu fac direct cast pt ca imi ramane metode si proprietati de la tipul superior
            
            if(animal.GetType().BaseType == typeof(Rechin))
            {
                Rechin r = animal as Rechin;
                return new Rechin(r.Name, r.Age, r.Speed, r.EatCategory, r.Depth, r.NrAripioare, r.Pradator, r.Comestibil, r.TypeOfWater, r.Lenght, r.NrDinti);
            }
            else if (animal.GetType().BaseType == typeof(Megalodon))
            {
                Megalodon p = animal as Megalodon;
                return new Megalodon(p.Name, p.Age, p.Speed, p.EatCategory, p.Depth, p.NrAripioare, p.Pradator, p.Comestibil, p.TypeOfWater, p.Lenght, p.NrDinti);
            }
            else if (animal.GetType().BaseType == typeof(Peste))
            {
                Peste p = animal as Peste;
                return new Peste(p.Name, p.Age, p.Speed, p.EatCategory, p.Depth, p.NrAripioare, p.Pradator, p.Comestibil, p.TypeOfWater, p.Lenght);
            }
            else
                return animal; // Nu are un base type mai primitiv
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals.Interfaces
{
    interface IAerian
    {
        int WingsDim { get; set; }

        int Altitude { get; set; }
    }
}

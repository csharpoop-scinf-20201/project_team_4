﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals;

namespace TheEvolutionGame.Administrate
{
    class World
    {
        private List<Animal> animals = new List<Animal>();

        public List<Animal>  Animals { get { return animals; } }

        public void Populate()
        {
            Turtle turtle = new Turtle("Testoasa", 3, 2, "Herbivore", 100, 4, "Sea", "Beach");
            animals.Add(turtle);

            Homar homar = new Homar("Homar", 1, 2, "Herbivore", 200, "Sea", 6, 2);
            animals.Add(homar);

            Megalodon rechin = new Megalodon("Rechin preistoric", 5, 50, "Carnivore", 300, 8, true, true, "Sea", 20, 300);
            animals.Add(rechin);

            AddAntilope();
            AddLebede();
            AddVulturi();
        }

        public void AddAnimal(Animal animal)
        {
            animals.Add(animal);
        }

        public void AddElephant() {
            Console.WriteLine("\n\nIntroduce an elephant in our list!");

            Console.WriteLine("Enter the name of the elephant:");
            string elefantName = Console.ReadLine();

            Console.WriteLine("Enter elephat age:");
            int elefantAge = int.Parse(Console.ReadLine());

            Console.WriteLine("Speed");
            int elefantSpeed = int.Parse(Console.ReadLine());

            Console.WriteLine("EatCategory:");
            string elefantEatCategory = Console.ReadLine();

            Console.WriteLine("Weight:");
            int elefantWeight = int.Parse(Console.ReadLine());

            Console.WriteLine("Gestation time:");
            int elefantGestation = int.Parse(Console.ReadLine());

            AddAnimal(new Elefant(elefantName, elefantAge, elefantSpeed, elefantEatCategory, elefantWeight, elefantGestation, "Savana"));

            Console.WriteLine("-------------------------------------------------------");
        }

        private void AddLebede()
        {
            AddAnimal(new Lebada("White Swan", 2, 80, "Erbivor", 10, "White", 3, 1, 300, "Surface of the water"));
            AddAnimal(new Lebada("Black Swan", 3, 70, "Erbivor", 11, "Black", 1, 1, 300, "Surface of the water"));
        }

        private void AddVulturi()
        {
            AddAnimal(new Vultur("Bald Eagle", 5, 120, "Carnivor", 2, "yes", 45, 2000, "rocks"));
            AddAnimal(new Vultur("Crwoed Eagle", 7, 140, "Carnivor", 1, "yes", 50, 1500, "rocks"));
        }

        private void AddAntilope()
        {
            AddAnimal(new Antilopa("Impala", 11, 80, "erbivor", 45, 7, 1, "Savana"));
            AddAnimal(new Antilopa("Antilopa-cal", 12, 84, "erbivor", 200, 8, 1, "Savana"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Turtle : VertebratTerestruAcvatic
    {
        public int NrPicioare { get; private set; }

        public Turtle() { }

        public Turtle(string Name, int Age, int Speed, string EatCategory, int Depth, int NrPicioare, string TypeOfWater, string LandForm) : base(Name, Age, Speed, EatCategory, Depth, TypeOfWater, LandForm)
        {
            this.NrPicioare = NrPicioare;
        }
        public override string Description()
        {
            return base.Description() + $"I have {NrPicioare} legs and I'm not a ninja.";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Animals.Interfaces;

namespace TheEvolutionGame.Animals
{
    class Peste : VertebratAcvatic
    {
        public bool Pradator { get; private set; }

        public bool Comestibil { get; private set; }
        
        public int NrAripioare { get; private set; }

        public virtual int Lenght { get; protected set; }

        public Peste(){}

        public Peste(string Name, int Age, int Speed, string EatCategory, int Depth, int NrAripioare, 
            bool Pradator, bool Comestibil, string TypeOfWater, int Lenght) : base(Name, Age, Speed, EatCategory, Depth, TypeOfWater)
        {
            this.Pradator = Pradator;
            this.Comestibil = Comestibil;
            this.NrAripioare = NrAripioare;
            this.Lenght = Lenght;
        }

        public override string Description()
        {
            return  base.Description() +  $" I am a predator {Pradator} and you can eat me {Comestibil}! I have {NrAripioare} fins and {Lenght} meters long.";
        }

        public void MakeBloop()
        {
            Console.WriteLine("Bloop!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class Rechin : Megalodon
    {
        private int lenght;

        public override int Lenght { get { return lenght; } protected set { if (value > 12) lenght = 12; else lenght = value; } }

        public Rechin() { }

        public Rechin(string Name, int Age, int Speed, string EatCategory, int Depth, int NrAripioare,
           bool Pradator, bool Comestibil, string TypeOfWater, int Lenght, int NrDinti) : base(Name, Age, Speed, EatCategory, Depth, NrAripioare, Pradator, Comestibil, TypeOfWater, Lenght, NrDinti)
        {
        }

        public override string Description()
        {
            return $"I am an aquatic animal with the name {Name}, I am smaller then my ancestor, I am alive for {Age} years and " +
                $"I live at the depth {Depth} meters in {TypeOfWater} water!" +
                $" I am the Megalodon predator {Pradator} and you can eat me {Comestibil} but I can eat you too because I have {NrDinti} teeth! I have {NrAripioare} fins and {Lenght} meters long. ";
        }

        public override void Attack()
        {
            Console.WriteLine("He ate you, even he is smaller!");
        }
    }
}

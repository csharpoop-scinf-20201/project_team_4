﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals.Interfaces
{
    interface IAnimal
    {
        string Description();

        int SpeedValue();

        void Friends();

        void Enemies();
    }
}

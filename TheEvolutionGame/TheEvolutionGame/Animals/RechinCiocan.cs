﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Animals
{
    class RechinCiocan : Rechin
    {
        public int DimensiuneIntreOchi { get; private set; }

        public RechinCiocan(){}

        public RechinCiocan (string Name, int Age, int Speed, string EatCategory, int Depth, int NrAripioare,
           bool Pradator, bool Comestibil, string TypeOfWater, int Lenght, int NrDinti, int DimensiuneIntreOchi) : base(Name, Age, Speed, EatCategory, Depth, NrAripioare, Pradator, Comestibil, TypeOfWater, Lenght, NrDinti)
        {

            this.DimensiuneIntreOchi = DimensiuneIntreOchi;
        }

        public override string Description()
        {
            return base.Description() +  $"Distance between the eyes is {DimensiuneIntreOchi} cm!";
        }
    }
}
